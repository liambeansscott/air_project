<?php
// We're using our hooks here! the wp_header() hook calls wp_enqueue_scripts, we've written a function and added it to the wp_enqueue_scripts so its called :)
function startwordpress_scripts() {
	wp_enqueue_style( 'bootstrap', get_template_directory_uri().'/css/bootstrap.min.css', array(), '3.3.6' );
	wp_enqueue_style( 'blog', get_template_directory_uri().'/css/blog.css' );
	wp_enqueue_script( 'bootstrap', get_template_directory_uri().'/js/bootstrap.min.js', array( 'jquery' ), '3.3.6', true );
}

add_action( 'wp_enqueue_scripts', 'startwordpress_scripts' );

// Add Google Fonts, this uses a url to add a style which is fun
function startwordpress_google_fonts() {
	wp_register_style('OpenSans', 'http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800');
	wp_enqueue_style( 'OpenSans');
}

add_action('wp_print_styles', 'startwordpress_google_fonts');

// WordPress Titles, allowing for title tag to get pulled rather than use the echo title in header
add_theme_support( 'title-tag' );

// Custom settings, this function adds a menu to our admin backend!
function custom_settings_add_menu() {
	add_menu_page( 'Custom Settings', 'Custom Settings', 'manage_options', 'custom-settings', 'custom_settings_page', null, 99 );
}
add_action( 'admin_menu', 'custom_settings_add_menu' );

// Create Custom Global Settings, this here is going to add some html to the settings page we just made above
function custom_settings_page() { ?>
	<div class="wrap">
		<h1>Custom Settings</h1>
		<form method="post" action="options.php">
				<?php
						settings_fields( 'section' );
						do_settings_sections( 'theme-options' );
						submit_button();
				?>
		</form>
	</div>
<?php }


// Instagram, add an instagram setting, we need this
function setting_instagram() { ?>
	<input type="text" name="instagram" id="instagram" value="<?php echo get_option( 'instagram' ); ?>" />
<?php }

// this set up allows us to show accept and save option fields, we then access these admin page values in our sidebar
function custom_settings_page_setup() {
	add_settings_section( 'section', 'All Settings', null, 'theme-options' );
	add_settings_field( 'instagram', 'instagram URL', 'setting_instagram', 'theme-options', 'section' );

	register_setting('section', 'instagram');
}
add_action( 'admin_init', 'custom_settings_page_setup' );

// Support Featured Images, this groovy plugin allows us to add thumbnails, in our content-single.php we can get the thumbnail and show it on the post
add_theme_support( 'post-thumbnails' );

// This function will allow us to make our own custom posts with our own fields, here im suggesting reports as a post type
function create_report() {
	register_post_type( 'report_post',
			array(
			'labels' => array(
	'name' => __( 'Report' ),
	'singular_name' => __( 'Report' ),
			),
			'public' => true,
			'has_archive' => true,
			'supports' => array(
	'title',
	'editor',
	'thumbnail',
	'report_fields'
			)
	));
}
add_action( 'init', 'create_report' );