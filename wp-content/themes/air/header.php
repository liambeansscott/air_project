<!-- So this is our header, it contains metadat about the site, and links through our peng css file -->

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
        <!-- Bootstrap core CSS -->





    <!-- So thhis is called a hook, we put it in our head and it calls a bunch of wp lifecycle methods and pulls some meta data from the backend -->
    <!-- That was a really handwavey explanation, but everyones seems to be like that, which is great because to be a true wordpress developer 0 understanding of php is essential ignorance -->
      <?php wp_head(); ?>

  </head>

  <body>

    <div class="blog-masthead">
      <div class="container">
        <nav class="blog-nav">
			<?php wp_list_pages( '&title_li=' ); ?>
        </nav>
      </div>
    </div>

    <div class="container">

      <div class="blog-header">
      <!-- So here we're pulling blogInfo from the back end and taking the name field and putting it here -->
      <!-- We're then whacking it in a link that takes us to the wpurl (our home) -->
        <h1 class="blog-title"><a href="<?php echo get_bloginfo( 'wpurl' );?>"><?php echo get_bloginfo( 'name' ); ?></a>
</h1>
        <p class="lead blog-description"><?php echo get_bloginfo('description')?></p>
      </div>