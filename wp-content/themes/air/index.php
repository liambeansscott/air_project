<!-- This then our home page, its pretty sparse, but we'll populate it with our fugazi php components -->
<!-- so get_header() get_sidebar() and get_footer() look for their respective php files, i think wordpress expects you to have these .php files -->
<?php get_header(); ?>

      <div class="row">

        <div class="col-sm-8 blog-main">
		<!-- so get_template_part( 'partials/content', 'page' ) would call 'arg1-arg2.php' in this case partials/content-page.php -->
		<!-- so getPostFormat() will append the type of postformat to the end. So we can render a diff .php for each type of post -->
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<?php get_template_part( 'content', get_post_format() ); ?>

  <!-- contents of the loop -->
		<?php endwhile; endif; ?>


          <nav>
            <ul class="pager">
              <li><?php previous_posts_link( 'Newer posts' ); ?></li>
              <li><?php next_posts_link( 'Older posts' ); ?> </li>
            </ul>
		  </nav>
		  


        </div><!-- /.blog-main -->
		<?php get_sidebar(); ?>

      

      </div><!-- /.row -->

   <?php get_footer(); ?>
