<!-- Another presentational looking component! -->
<!-- Content is where our blog posts go :) -->
<!-- This will be loaded into .php, then when you click it it will load a single-> content-single -->
<!-- It isnt really presentational anymore, its like calling some functions to pull data fromt he backend! -->
<div class="blog-post">
  <!-- So we have turned the title of each blog post into a link which is the permalink property of each post -->
  <script >console.log('blog post loaded')</script>

  <h2 class="blog-post-title"><a href=<?php the_permalink()?>><?php the_title(); ?></a></h2>
  <p class="blog-post-meta"><?php the_date()?> by <?php the_author()?> </p>
  <?php if ( has_post_thumbnail() ) {?>
	<div class="row">
		<div class="col-md-4">
			<?php	the_post_thumbnail('thumbnail'); ?>
		</div>
		<div class="col-md-6">
			<?php the_excerpt(); ?>
		</div>
	</div>
	<?php } else { ?>
	<?php the_excerpt(); ?>
<?php } ?>
  <a href="<?php comments_link(); ?>">
    <?php
      printf( _nx( 'One Comment', '%1$s Comments', get_comments_number(), 'comments title', 'textdomain' ), number_format_i18n( 		get_comments_number() ) ); 
      ?>
  </a>
            
 </div><!-- /.blog-post -->